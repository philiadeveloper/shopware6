<?php
/**
 * Created by PhpStorm.
 * User: Philia Solutions
 * Date: 26-10-2018
 * Time: 11:48
 */

Class Shopware_Plugins_Frontend_FrontPage_Bootstrap extends Shopware_Components_Plugin_Bootstrap{
    public function getInfo()
    {
        return[
            'version'=>'1.0.0','label'=>'Frontpage Management'
        ];
    }
    public function update($version)
    {
        if($version=='1.0.0'){

        };
        return true;
    }

    public function install()
    {
        return true;
    }
    public function uninstall()
    {
        return true;
    }
    public function enable()
    {
        return parent::enable();
    }
    public function disable()
    {
        return parent::disable();
    }
}