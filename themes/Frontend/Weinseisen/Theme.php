<?php

namespace Shopware\Themes\Weinseisen;

use Shopware\Components\Form as Form;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';
	
    protected $name = <<<'SHOPWARE_EOD'
Weinseisen
SHOPWARE_EOD;

    protected $description = <<<'SHOPWARE_EOD'
Weinseisen
SHOPWARE_EOD;

    protected $author = <<<'SHOPWARE_EOD'
Priyavrat (Philia Solutions)
SHOPWARE_EOD;

    protected $license = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;
//protected $css = array('src/css/style.css');
protected $css = array('src/css/style.css','src/css/owl.carousel.min','src/css/owl.theme.default.min');
 
public function createConfig(Form\Container\TabContainer $container)
    {
    }
}