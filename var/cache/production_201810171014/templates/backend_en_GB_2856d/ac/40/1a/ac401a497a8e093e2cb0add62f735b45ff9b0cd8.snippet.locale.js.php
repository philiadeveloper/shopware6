<?php /* Smarty version Smarty-3.1.12, created on 2018-11-01 06:03:49
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\login\store\locale.js" */ ?>
<?php /*%%SmartyHeaderCode:119055bda8935a0b725-64116254%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ac401a497a8e093e2cb0add62f735b45ff9b0cd8' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\login\\store\\locale.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '119055bda8935a0b725-64116254',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bda8935a86b34_69670255',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bda8935a86b34_69670255')) {function content_5bda8935a86b34_69670255($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Login
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
Ext.define('Shopware.apps.Login.store.Locale', {
    extend: 'Ext.data.Store',
    autoLoad: true,
    model : 'Shopware.apps.Login.model.Locale'
});
<?php }} ?>