<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 11:02:31
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\article_list\model\backup.js" */ ?>
<?php /*%%SmartyHeaderCode:34705bd97db7459368-37002548%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4295530113a57ba7fd92eba5bd1d548c4dfd1782' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\article_list\\model\\backup.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '34705bd97db7459368-37002548',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd97db7564218_74355927',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd97db7564218_74355927')) {function content_5bd97db7564218_74355927($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Shopware Model - backup model
 *
 * Represents a single backup
 */
//
Ext.define('Shopware.apps.ArticleList.model.Backup', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
        //
        { name: 'id', type: 'int', useNull: true },
        { name: 'date', type: 'datetime', dateFormat: 'Y-m-d H:i:s' },
        { name: 'items', type: 'int' },
        { name: 'size', type: 'int' },
        { name: 'path', type: 'string' },
        { name: 'filterString', type: 'string' },
        { name: 'operationString', type: 'string' }
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            destroy: '<?php echo '/development/shopware2/backend/ArticleList/delete';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'data',
            totalProperty:'total'
        }
    }

});
//
<?php }} ?>