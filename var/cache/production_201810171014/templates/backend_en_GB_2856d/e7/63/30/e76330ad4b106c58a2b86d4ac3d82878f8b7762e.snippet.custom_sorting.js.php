<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:46:48
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\base\store\custom_sorting.js" */ ?>
<?php /*%%SmartyHeaderCode:78745bd97a08086b89-53060399%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e76330ad4b106c58a2b86d4ac3d82878f8b7762e' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\base\\store\\custom_sorting.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '78745bd97a08086b89-53060399',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd97a0808fd24_73425708',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd97a0808fd24_73425708')) {function content_5bd97a0808fd24_73425708($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

Ext.define('Shopware.apps.Base.store.CustomSorting', {
    extend:'Shopware.store.Listing',

    sorters: [{
        property: 'position',
        direction: 'ASC'
    }],

    configure: function() {
        return {
            controller: 'CustomSorting'
        };
    },
    model: 'Shopware.apps.Base.model.CustomSorting'
});

//
<?php }} ?>