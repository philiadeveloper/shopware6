<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:14
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\model\setting.js" */ ?>
<?php /*%%SmartyHeaderCode:306365bd94fb6b482e1-27896530%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '695a8ac415405bf7cd2355abb4d5778f04c51d5f' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\model\\setting.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '306365bd94fb6b482e1-27896530',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb6b532b8_00393993',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb6b532b8_00393993')) {function content_5bd94fb6b532b8_00393993($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

//
Ext.define('Shopware.apps.MediaManager.model.Setting', {
    extend: 'Ext.data.Model',
    fields: [
        //
        {
            name: 'displayType',
            type: 'string'
        }, {
            name: 'itemsPerPage',
            type: 'number'
        }, {
            name: 'tableThumbnailSize',
            type: 'number'
        }, {
            name: 'gridThumbnailSize',
            type: 'number'
        }
    ]
});
//
<?php }} ?>