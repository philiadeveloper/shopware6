<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:16
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\view\batch_move\batch_move.js" */ ?>
<?php /*%%SmartyHeaderCode:218055bd94fb80a1476-51801283%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ffba65153e03a23be616d1c27efbb6ea8f2b9b3' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\batch_move\\batch_move.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '218055bd94fb80a1476-51801283',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb80d20d6_95939841',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb80d20d6_95939841')) {function content_5bd94fb80d20d6_95939841($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//
//
Ext.define('Shopware.apps.MediaManager.view.batchMove.BatchMove', {
    extend: 'Enlight.app.SubWindow',
    alias: 'widget.batchMove.BatchMove',

    footerButton: false,
    modal: true,

    height: 135,
    width: 400,

    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'move/media/title','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/media/title','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Moving media files<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/media/title','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    indicatorSnippet: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'move/indicator/snippet','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/indicator/snippet','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
[0] of [1]<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/indicator/snippet','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    initComponent: function() {
        var me = this;

        me.isCanceled = false;
        me.maxIndex = me.mediasToMove.length - 1;
        me.currentIndex = 0;

        me.items = me.createItems();
        me.dockedItems = me.createDockedItems();

        me.callParent(arguments);
    },

    /**
     * After render init's the move media batch process
     */
    afterRender: function() {
        var me = this;

        me.callParent(arguments);

        me.startMoveMedia();
    },

    /**
     * @return { Ext.ProgressBar }
     */
    createItems: function() {
        var me = this;

        me.progressBar = Ext.create('Ext.ProgressBar', {
            margin: 20,
            text: Ext.String.format(me.indicatorSnippet, 0, '' + me.mediasToMove.length)
        });

        return me.progressBar;
    },

    /**
     * @return { Ext.toolbar.Toolbar }
     */
    createDockedItems: function() {
        var me = this;

        return Ext.create('Ext.toolbar.Toolbar', {
            ui: 'shopware-ui',
            dock: 'bottom',
            items: me.createToolbarItems()
        });
    },

    /**
     * @return { Array }
     */
    createToolbarItems: function() {
        var me = this;

        return [
            '->',
            me.createCancelButton()
        ]
    },

    /**
     * @return { Ext.button.Button }
     */
    createCancelButton: function() {
        var me = this;

        return Ext.create('Ext.button.Button', {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'move/media/cancel','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/media/cancel','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/media/cancel','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            handler: Ext.bind(me.onCancelClick, me)
        });
    },

    /**
     * on cancel click set the property isCanceled to true,
     * so we can break the move media process
     *
     * @param { Ext.button.Button } button
     */
    onCancelClick: function(button) {
        var me = this;

        me.isCanceled = true;

        me.updateProgressBar(true);
        button.setDisabled(true);
    },

    startMoveMedia: function() {
        var me = this,
            store = Ext.create('Shopware.apps.MediaManager.store.Media');

        store.add(me.mediasToMove[me.currentIndex]);

        store.sync({
            callback: Ext.bind(me.updateInternal, me)
        });
    },

    updateInternal: function() {
        var me = this;

        me.currentIndex++;
        me.updateProgressBar();
        me.afterUpdateProgressBar();
    },

    updateProgressBar: function(force) {
        var me = this,
            text = Ext.String.format(me.indicatorSnippet, me.currentIndex, '' + me.mediasToMove.length),
            value = (1 / me.mediasToMove.length) * (me.currentIndex),
            force = force || false;

        if (force || me.isCanceled) {
            me.progressBar.updateProgress(value, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'move/media/cancel_message','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/media/cancel_message','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancelling process...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'move/media/cancel_message','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', true);
            return;
        }

        me.progressBar.updateProgress(value, text, true);
    },

    /**
     * Checks if progress is ready or canceled, else start a new process.
     */
    afterUpdateProgressBar: function() {
        var me = this;

        if (me.isCanceled || me.currentIndex > me.maxIndex) {
            me.closeWindow();
            return;
        }

        me.startMoveMedia();
    },

    closeWindow: function() {
        var me = this;

        if (me.progressBar.getActiveAnimation()) {
            Ext.defer(me.closeWindow, 200, me);
            return;
        }

        // Wait a little before destroy the window for a better use feeling
        Ext.defer(me.destroy, 500, me);
        me.updateSourceView();
    },

    /**
     * Updates the MediaManager window. Set is loading to false, reloads the store and fires a refresh event
     * to the albumTree to update the treeView.
     */
    updateSourceView: function() {
        var me = this;

        me.mediaGrid.setLoading(false);
        me.mediaView.setLoading(false);
        me.mediaGrid.getStore().load();

        me.sourceView.fireEvent('refresh', me.sourceView);
    }
});
//
<?php }} ?>