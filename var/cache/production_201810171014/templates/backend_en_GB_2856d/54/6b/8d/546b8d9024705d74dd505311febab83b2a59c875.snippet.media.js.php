<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:14
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\model\media.js" */ ?>
<?php /*%%SmartyHeaderCode:167355bd94fb6a57684-02957395%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '546b8d9024705d74dd505311febab83b2a59c875' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\model\\media.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167355bd94fb6a57684-02957395',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb6a8b875_37241120',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb6a8b875_37241120')) {function content_5bd94fb6a8b875_37241120($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.MediaManager.model.Media', {
    extend: 'Ext.data.Model',
    fields: [
        //
        'created',
        'description',
        'extension',
        'id',
        { name: 'name', sortType: 'asUCText' },
        'type',
        'path',
        'userId',
        'thumbnail',
        'thumbnails',
        'width',
        'height',
        'albumId',
        'newAlbumID',
        'virtualPath',
        'timestamp'
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: '<?php echo '/development/shopware2/backend/MediaManager/getAlbumMedia';?>',
            create: '<?php echo '/development/shopware2/backend/MediaManager/saveMedia';?>',
            update: '<?php echo '/development/shopware2/backend/MediaManager/saveMedia/targetField/media';?>',
            destroy: '<?php echo '/development/shopware2/backend/MediaManager/removeMedia';?>'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    }
});
//

<?php }} ?>