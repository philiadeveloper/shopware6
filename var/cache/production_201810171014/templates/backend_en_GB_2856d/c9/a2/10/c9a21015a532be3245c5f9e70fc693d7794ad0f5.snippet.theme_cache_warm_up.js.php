<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:46:52
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\index\model\theme_cache_warm_up.js" */ ?>
<?php /*%%SmartyHeaderCode:154745bd97a0c626eb0-72549029%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c9a21015a532be3245c5f9e70fc693d7794ad0f5' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\index\\model\\theme_cache_warm_up.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '154745bd97a0c626eb0-72549029',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd97a0c62d721_30914301',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd97a0c62d721_30914301')) {function content_5bd97a0c62d721_30914301($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Theme cache warm up model
 *
 * Loads stores that use themes
 */
//
Ext.define('Shopware.apps.Index.model.ThemeCacheWarmUp', {
    extend: 'Shopware.apps.Base.model.Shop'
});
//
<?php }} ?>