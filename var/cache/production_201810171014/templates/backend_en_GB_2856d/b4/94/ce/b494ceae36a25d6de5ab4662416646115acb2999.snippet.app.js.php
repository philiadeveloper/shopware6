<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:47:00
         compiled from "C:\xampp2\htdocs\development\shopware2\custom\plugins\SwagExtendCustomer\Resources\views\backend\swag_extend_customer\app.js" */ ?>
<?php /*%%SmartyHeaderCode:59525bd97a144b99f3-85327236%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b494ceae36a25d6de5ab4662416646115acb2999' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\custom\\plugins\\SwagExtendCustomer\\Resources\\views\\backend\\swag_extend_customer\\app.js',
      1 => 1540879197,
      2 => 'file',
    ),
    '114c77501c07b9f334ca121a408a462f9ff8823a' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\customer\\app.js',
      1 => 1539751512,
      2 => 'file',
    ),
    'e8612d75f22d014ea91c287bb84258765ce6bee3' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\custom\\plugins\\SwagExtendCustomer\\Resources\\views\\backend\\swag_extend_customer\\controller\\my_own_controller.js',
      1 => 1540476581,
      2 => 'snippet',
    ),
    'cc95a9180f82725bd98efa7a637ddc93352e4fbf' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\custom\\plugins\\SwagExtendCustomer\\Resources\\views\\backend\\swag_extend_customer\\view\\detail\\my_own_tab.js',
      1 => 1540476581,
      2 => 'snippet',
    ),
  ),
  'nocache_hash' => '59525bd97a144b99f3-85327236',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'currency' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd97a146904d8_74253914',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd97a146904d8_74253914')) {function content_5bd97a146904d8_74253914($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Customer
 * @subpackage App
 * @version    $Id$
 * @author shopware AG
 */

// 

/**
 * Shopware Application - Customer list backend module
 *
 * Contains the configuration for the customer list backend module.
 * This component defines which controllers belong to the application or whether the bulk loading is activated.
 */
// 
// 
Ext.define('Shopware.apps.Customer', {

    /**
     * The name of the module. Used for internal purpose
     * @string
     */
    name: 'Shopware.apps.Customer',

    /**
     * Extends from our special controller, which handles the sub-application behavior and the event bus
     * @string
     */
    extend: 'Enlight.app.SubApplication',

    /**
     * Enable bulk loading
     * @boolean
     */
    bulkLoad: true,

    /**
     * Sets the loading path for the sub-application.
     *
     * @string
     */
    loadPath: '<?php echo '/development/shopware2/backend/customer/load';?>',

    /**
     * Requires controllers for sub-application
     * @array
     */
    controllers: [ 'Detail', 'Order', 'Main', 'Stream' ],

    /**
     * The detail controller knows all form field sets and the detail window component
     * @array
     */
    views: [

        'main.QuickView',
        'main.StreamView',
        'main.CustomerList',
        'main.CustomerListFilter',
        'main.Window',
        'main.Wizard',

        'detail.Window',
        'detail.Base',
        'detail.Debit',
        'detail.Comment',
        'detail.Additional',
        'order.List',
        'order.Chart',
        'address.List',
        'address.detail.Window',
        'address.detail.Address',

        'chart.AmountChartFactory',
        'chart.Chart',
        'chart.MetaChart',

        'customer_stream.Detail',
        'customer_stream.Listing',
        'customer_stream.Preview',
        'customer_stream.ConditionPanel',
        'customer_stream.Assignment',
        'customer_stream.ConditionField',
        'customer_stream.conditions.AgeCondition',
        'customer_stream.conditions.HasAddressWithCountryCondition',
        'customer_stream.conditions.HasCanceledOrdersCondition',
        'customer_stream.conditions.HasNewsletterRegistrationCondition',
        'customer_stream.conditions.IsCustomerSinceCondition',
        'customer_stream.conditions.IsInCustomerGroupCondition',
        'customer_stream.conditions.HasOrderCountCondition',
        'customer_stream.conditions.OrderedAtWeekdayCondition',
        'customer_stream.conditions.OrderedInLastDaysCondition',
        'customer_stream.conditions.OrderedInShopCondition',
        'customer_stream.conditions.RegisteredInShopCondition',
        'customer_stream.conditions.OrderedOnDeviceCondition',
        'customer_stream.conditions.OrderedProductCondition',
        'customer_stream.conditions.OrderedProductOfCategoryCondition',
        'customer_stream.conditions.OrderedProductOfManufacturerCondition',
        'customer_stream.conditions.OrderedWithDeliveryCondition',
        'customer_stream.conditions.OrderedWithPaymentCondition',
        'customer_stream.conditions.HasTotalOrderAmountCondition',
        'customer_stream.conditions.CustomerAttributeCondition',
        'customer_stream.conditions.SalutationCondition',
        'customer_stream.conditions.SearchTermCondition',
        'customer_stream.conditions.field.AttributeValue',
        'customer_stream.conditions.field.AttributeWindow',
        'customer_stream.conditions.field.OperatorField'
    ],

    /**
     * All required stores are defined here. The detail store contains all data around the customer.
     * The other shops are global stores which used for combo boxes.
     * @array
     */
    stores: [ 'Detail', 'Orders', 'Chart', 'Batch', 'Address', 'QuickView', 'Preview', 'CustomerStream' ],

    /**
     * All store's required models. The detail store handles the base, billing, shipping and debit model.
     * @array
     */
    models: [ 'Customer', 'Debit', 'PaymentData', 'Order', 'Chart', 'Batch', 'Address', 'QuickView', 'CustomerStream' ],

    /**
     * Returns the main application window for this is expected
     * by the Enlight.app.SubApplication class.
     * The class sets a new event listener on the "destroy" event of
     * the main application window to perform the destroying of the
     * whole sub application when the user closes the main application window.
     *
     * This method will be called when all dependencies are solved and
     * all member controllers, models, views and stores are initialized.
     *
     * @private
     * @return [object] mainWindow - the main application window based on Enlight.app.Window
     */
    launch: function() {
        var me = this,
            mainController = me.getController('Main');

        me.currencySign = '<?php echo $_smarty_tpl->tpl_vars['currency']->value['sign'];?>
';
        me.currencyAtEnd = '<?php echo $_smarty_tpl->tpl_vars['currency']->value['currencyAtEnd'];?>
';

        return mainController.mainWindow;
    }
});
// 
// <?php /*  Call merged included template "backend/swag_extend_customer/controller/my_own_controller.js" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("backend/swag_extend_customer/controller/my_own_controller.js", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '59525bd97a144b99f3-85327236');
content_5bd97a145d3f50_32053197($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "backend/swag_extend_customer/controller/my_own_controller.js" */?>
// <?php /*  Call merged included template "backend/swag_extend_customer/view/detail/my_own_tab.js" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("backend/swag_extend_customer/view/detail/my_own_tab.js", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '59525bd97a144b99f3-85327236');
content_5bd97a1462fd20_20099463($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "backend/swag_extend_customer/view/detail/my_own_tab.js" */?>
//
<?php }} ?><?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:47:00
         compiled from "C:\xampp2\htdocs\development\shopware2\custom\plugins\SwagExtendCustomer\Resources\views\backend\swag_extend_customer\controller\my_own_controller.js" */ ?>
<?php if ($_valid && !is_callable('content_5bd97a145d3f50_32053197')) {function content_5bd97a145d3f50_32053197($_smarty_tpl) {?>
Ext.define('Shopware.apps.SwagExtendCustomer.controller.MyOwnController', {
    /**
     * Override the customer main controller
     * @string
     */
    override: 'Shopware.apps.Customer.controller.Main',

    init: function () {
        var me = this;

        // me.callParent will execute the init function of the overridden controller
        me.callParent(arguments);
    }
});
<?php }} ?><?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:47:00
         compiled from "C:\xampp2\htdocs\development\shopware2\custom\plugins\SwagExtendCustomer\Resources\views\backend\swag_extend_customer\view\detail\my_own_tab.js" */ ?>
<?php if ($_valid && !is_callable('content_5bd97a1462fd20_20099463')) {function content_5bd97a1462fd20_20099463($_smarty_tpl) {?>// This tab will be shown in the customer module

Ext.define('Shopware.apps.SwagExtendCustomer.view.detail.MyOwnTab', {
    extend: 'Ext.container.Container',
    padding: 10,
    title: 'MyOwnTab',

    initComponent: function() {
        var me = this;

        me.items  =  [{
            xtype: 'label',
            html: '<h1>Hello world</h1>'
        }];

        me.callParent(arguments);
    }

});<?php }} ?>