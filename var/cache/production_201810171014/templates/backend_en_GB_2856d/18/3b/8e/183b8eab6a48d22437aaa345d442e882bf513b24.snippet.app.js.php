<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:47:01
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\benchmark\app.js" */ ?>
<?php /*%%SmartyHeaderCode:299685bd97a154a9921-02359285%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '183b8eab6a48d22437aaa345d442e882bf513b24' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\benchmark\\app.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '299685bd97a154a9921-02359285',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd97a15598d13_20787599',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd97a15598d13_20787599')) {function content_5bd97a15598d13_20787599($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

Ext.define('Shopware.apps.Benchmark', {
    name: 'Shopware.apps.Benchmark',
    extend:'Enlight.app.SubApplication',
    bulkLoad: true,
    loadPath: '<?php echo '/development/shopware2/backend/Benchmark/load';?>',

    views: ['overview.Window', 'settings.Window', 'settings.IndustryField', 'settings.IndustryWindow'],
    models: [ 'ShopConfig' ],
    stores: [ 'Industry', 'ShopConfigs' ],

    controllers: [ 'Main', 'Settings' ],

    launch: function() {
        var me = this,
            mainController = me.getController('Main');

        return mainController.mainWindow;
    }
});
<?php }} ?>