<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:14
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\view\album\add.js" */ ?>
<?php /*%%SmartyHeaderCode:31895bd94fb6bae498-29008012%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '720ca59334277996f1224c4bd95f427ac39cc715' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\album\\add.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31895bd94fb6bae498-29008012',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb6c53c20_26417572',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb6c53c20_26417572')) {function content_5bd94fb6c53c20_26417572($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage View
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Album Add
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.MediaManager.view.album.Add', {
    extend: 'Ext.window.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'addAlbumTitle','default'=>'Mediamanager - Add Album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbumTitle','default'=>'Mediamanager - Add Album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Media manager - Add album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbumTitle','default'=>'Mediamanager - Add Album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.mediamanager-album-add',
    border: false,
    width: 400,
    autoShow: true,
    parentId: null,
    snippets: {
        albumName: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'addAlbum/albumName','default'=>'Album name','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbum/albumName','default'=>'Album name','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Album name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbum/albumName','default'=>'Album name','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        add: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'addAlbum/add','default'=>'Add album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbum/add','default'=>'Add album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbum/add','default'=>'Add album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        cancel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'addAlbum/cancel','default'=>'Cancel','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbum/cancel','default'=>'Cancel','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'addAlbum/cancel','default'=>'Cancel','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    initComponent: function() {
        var me = this;

        me.nameField = Ext.create('Ext.form.field.Text', {
            fieldLabel: me.snippets.albumName,
            labelWidth: 200,
            name: 'text',
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
                scope: me,
                keypress: function(textfield, e) {
                    if (e.getKey() === Ext.EventObject.ENTER && textfield.isValid()) {
                        me.down('#savebtn').fireEvent('click', me.down('#savebtn'));
                    }
                }
            }
        });

        me.formPanel = Ext.create('Ext.form.Panel', {
            bodyPadding: 12,
            defaults: {
                labelStyle: 'font-weight: 700'
            },
            items: [ me.nameField ]
        });

        // If we're adding a sub album, we need the parent id
        if(me.parentId) {
            me.parentIdField = Ext.create('Ext.form.field.Hidden', {
                name: 'parentId',
                value: me.parentId
            });
            me.formPanel.add(me.parentIdField);
        }

        me.items = [ me.formPanel ];
        me.dockedItems = [{
           xtype: 'toolbar',
           dock: 'bottom',
           ui: 'shopware-ui',
           cls: 'shopware-toolbar',
           items: me.createActionButtons()
        }];

        me.on('show', function() {
            me.nameField.focus(false, 200);
        });

        me.callParent(arguments);
    },

    createActionButtons: function() {
        var me = this;

        me.closeBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.cancel,
            cls: 'secondary',
            handler: function(btn) {
                var win = btn.up('window');
                win.destroy();
            }
        });

        this.addBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.add,
            itemId: 'savebtn',
            action: 'mediamanager-album-add-add',
            cls: 'primary'
        });

        return [ '->', this.closeBtn, this.addBtn ];
    }
});
//
<?php }} ?>