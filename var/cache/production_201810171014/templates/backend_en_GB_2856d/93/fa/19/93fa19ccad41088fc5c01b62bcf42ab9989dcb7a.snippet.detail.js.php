<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 10:47:17
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\article_list\model\detail.js" */ ?>
<?php /*%%SmartyHeaderCode:235065bd97a2566bbe6-50988000%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '93fa19ccad41088fc5c01b62bcf42ab9989dcb7a' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\article_list\\model\\detail.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '235065bd97a2566bbe6-50988000',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd97a25738a86_52231771',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd97a25738a86_52231771')) {function content_5bd97a25738a86_52231771($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Shopware Model - Detail models.
 * This dummy model will be overwritten by dynamically created model.
 * We need to provide a dummy model until that as we cannot specify a non
 * existing model in the store
 */
//
Ext.define('Shopware.apps.ArticleList.model.Detail', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model'
});
//
<?php }} ?>