<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:15
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\view\album\tree.js" */ ?>
<?php /*%%SmartyHeaderCode:52435bd94fb716cd28-68400901%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '28bf94ba3cf251426dda7642b7e19ad13658d8fa' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\album\\tree.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '52435bd94fb716cd28-68400901',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb7221bc5_59843673',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb7221bc5_59843673')) {function content_5bd94fb7221bc5_59843673($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage View
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Album Tree
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.MediaManager.view.album.Tree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.mediamanager-album-tree',
    region: 'west',
    width: 230,
    rootVisible: false,
    singleExpand: true,

    /**
     * Indicates if the toolbars should be created
     * @boolean
     */
    createToolbars: true,

    // Plugin to reorder the albums
    viewConfig: {
        plugins: {
            ptype: 'treeviewdragdrop'
        }
    },

    snippets:{
        tree:{
            columns:{
                album: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/columns/album','default'=>'Album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/album','default'=>'Album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/album','default'=>'Album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                files: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/columns/files','default'=>'Files','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/files','default'=>'Files','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Files<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/files','default'=>'Files','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                action: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/columns/action','default'=>'Action','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/action','default'=>'Action','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Action<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/action','default'=>'Action','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                editAlbumSettings: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/columns/editAlbumSettings','default'=>'Edit the album settings','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/editAlbumSettings','default'=>'Edit the album settings','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Edit album settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/columns/editAlbumSettings','default'=>'Edit the album settings','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            },
            searchAlbum: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/searchAlbum','default'=>'Search album...','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/searchAlbum','default'=>'Search album...','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Search for an album...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/searchAlbum','default'=>'Search album...','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            addAlbum: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/addAlbum','default'=>'Add album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/addAlbum','default'=>'Add album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/addAlbum','default'=>'Add album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            deleteAlbum: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/deleteAlbum','default'=>'Delete album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/deleteAlbum','default'=>'Delete album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Delete album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/deleteAlbum','default'=>'Delete album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            createSubAlbum: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/createSubAlbum','default'=>'Create subalbum','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/createSubAlbum','default'=>'Create subalbum','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Create subalbum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/createSubAlbum','default'=>'Create subalbum','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            settings: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/settings','default'=>'Settings','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/settings','default'=>'Settings','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/settings','default'=>'Settings','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            newAlbum: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/newAlbum','default'=>'Create new Album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/newAlbum','default'=>'Create new Album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Create new album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/newAlbum','default'=>'Create new Album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            refresh: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/refresh','default'=>'Refresh list','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/refresh','default'=>'Refresh list','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Refresh list<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/refresh','default'=>'Refresh list','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            emptyTrash: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree/emptyTrash','default'=>'Empty trash','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/emptyTrash','default'=>'Empty trash','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Empty recycle bin<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree/emptyTrash','default'=>'Empty trash','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and sets the toolbars
     * and the neccessary event listener
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        // Set column model and selection model
        me.columns = me.createColumns();
        me.selModel = Ext.create('Ext.selection.RowModel', {
            allowDeselect: true,
            listeners: {
                scope: me,
                select: me.onUnlockDeleteBtn
            }
        });

        // Create toolbars
        if(me.createToolbars) {
            me.tbar = me.createSearchToolbar();
            me.bbar = me.createActionToolbar();
        }
        me.on({
            itemcontextmenu: me.onOpenItemContextMenu,
            containercontextmenu: me.onOpenContainerContextMenu,
            render: me.initializeTreeDropZone,
            scope: me
        });

        // Add events for the context menu's
        me.addEvents(
            'addSubAlbum',
            'deleteAlbum',
            'editSettings',
            'addAlbum',
            'reload',
            'refresh',
            'emptyTrash'
        );

        // Select the correct node if we're in the media selection
        me.store.on('load', function() {
            if (me.store.getProxy().extraParams && me.store.getProxy().extraParams.albumId) {
                var record = me.store.getById(
                    me.store.getProxy().extraParams.albumId
                );
                if (record) {
                    me.getSelectionModel().select(record);
                    me.fireEvent('itemclick', this, record);
                }
            }
        }, me, { single: true });

        me.callParent(arguments);
    },

    /**
     * Creates the column model for the TreePanel
     *
     * @return [array] columns - generated columns
     */
    createColumns: function() {
        var me = this;

        var columns = [{
            xtype: 'treecolumn',
            text: me.snippets.tree.columns.album,
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }, {
            xtype: 'templatecolumn',
            text: me.snippets.tree.columns.files,
            flex: 1,
            sortable: true,
            dataIndex: 'mediaCount',
            tpl: Ext.create('Ext.XTemplate', '{mediaCount}')
        },
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        {
            xtype: 'actioncolumn',
            width: 50,
            handler: function(view, rowIndex, colIndex, item, e) {
                var target = e.getTarget();
                var nodeId = target.parentNode.parentNode.parentNode.viewRecordId;
                var record = me.store.getNodeById(nodeId);

                if (record.getId() == -13) {
                    me.fireEvent('emptyTrash', me, view, record);
                } else {
                    me.fireEvent('editSettings', me, view, record);
                }
            },
            header: me.snippets.tree.columns.action,
            items: [{
                iconCls: 'sprite-gear--arrow',
                style: 'width: 16px; height:16px',
                qtip: me.snippets.tree.columns.editAlbumSettings
            }],
            renderer: function(value, meta, record) {
                if (record.getId() == -13) {
                    me.columns[2].items[0].iconCls = 'sprite-minus-circle-frame';
                } else {
                    me.columns[2].items[0].iconCls = 'sprite-gear--arrow';
                }
            }
        }
        /* <?php }?> */
        ];

        return columns;
    },

    /**
     * Creates the search toolbar and the search field to filter
     * the albums.
     *
     * @return [object] generated Ext.toolbar.Toolbar
     */
    createSearchToolbar: function() {
        var me = this;
        this.searchField = Ext.create('Ext.form.field.Text', {
            emptyText: me.snippets.tree.searchAlbum,
            cls: 'searchfield',
            enableKeyEvents: true,
            width: 196,
            checkChangeBuffer: 500,
            action: 'mediamanager-album-tree-search'
        });

        return Ext.create('Ext.toolbar.Toolbar', {
            height: 35,
            items: [
                { xtype: 'tbspacer', width: 22 },
                this.searchField,
                { xtype: 'tbspacer', width: 4 }
            ]
        });
    },

    /**
     * Creates the action toolbar which includes the "add album"
     * and "delete album" buttons.
     *
     * @return [object] generated Ext.toolbar.Toolbar
     */
    createActionToolbar: function() {
        var me = this;
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?> */
        me.addBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.tree.addAlbum,
            cls: 'small secondary',
            action: 'mediamanager-album-tree-add'
        });
        /* <?php }?> */

        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?> */
        me.deleteBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.tree.deleteAlbum,
            cls: 'small secondary',
            action: 'mediamanager-album-tree-delete',
            disabled: true
        });
        /* <?php }?> */

        return Ext.create('Ext.toolbar.Toolbar', {
            items: [
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4){?> */
                '',
                me.addBtn,
        /* <?php }?> */
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp5=ob_get_clean();?><?php if ($_tmp5){?> */
                '->',
                me.deleteBtn,
                ''
        /* <?php }?> */
            ]
        });
    },

    /**
     * Event listener method which fires when the user selects a
     * node in the Ext.tree.Panel.
     *
     * Unlocks the "delete album"-button in the action toolbar.
     *
     * @event select
     * @param [object] selModel - The Ext.selection.RowModel of the Ext.tree.Panel
     * @param [object] record - Associated Ext.data.Model for the clicked item
     * @return void
     */
    onUnlockDeleteBtn: function(selModel, record) {
        var sel = selModel.getSelection(),
            id = ~~(1 * record.get('id'));

        if(sel.length > 0 && id > 0 && record.data.leaf == true) {
            if(this.deleteBtn) {
                this.deleteBtn.setDisabled(false);
            }

            return true;
        }

        if(this.deleteBtn) {
            this.deleteBtn.setDisabled(true);
        }
    },

    /**
     * Event listener method which fires when the user performs a right click
     * on a node in the Ext.tree.Panel.
     *
     * Opens a context menu which features functions to create a new sub album,
     * to delete the selected album and to open the album settings.
     *
     * Fires the following events on the Ext.tree.Panel:
     * - addSubAlbum
     * - deleteAlbum
     * - editSettings
     *
     * @event itemcontextmenu
     * @param [object] view - HTML DOM Object of the Ext.tree.Panel
     * @param [object] record - Associated Ext.data.Model for the clicked node
     * @param [object] item HTML DOM Object of the clicked node
     * @param [integer] index - Index of the clicked node in the associated Ext.data.TreeStore
     * @param [object] event - The fired Ext.EventObject
     * @return
     */
    onOpenItemContextMenu: function(view, record, item, index, event) {
        event.preventDefault(true);

        var me = this,
            nodeId = ~~(1 * record.get('id')),
            disableStatus = (nodeId > 0 && record.data.leaf == true) ? false : true,
            isTrash = record.data.id == -13;

        var menuItems = [
            /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp6=ob_get_clean();?><?php if ($_tmp6){?> */
            {
                text: me.snippets.tree.createSubAlbum,
                iconCls: 'sprite-photo-album--plus',
                handler: function() {
                    me.fireEvent('addSubAlbum', me, view, record, item, index);
                }
            },
            /* <?php }?> */
            /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp7=ob_get_clean();?><?php if ($_tmp7){?> */
            {
                text: me.snippets.tree.deleteAlbum,
                iconCls: 'sprite-photo-album--minus',
                disabled: disableStatus,
                handler: function() {
                    me.fireEvent('deleteAlbum', me, view, record, item, index);
                }
            },
            /* <?php }?> */
            /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp8=ob_get_clean();?><?php if ($_tmp8){?> */
            {
                text: me.snippets.tree.settings,
                iconCls: 'sprite-gear--arrow',
                handler: function() {
                    me.fireEvent('editSettings', me, view, record, item, index);
                }
            }
            /* <?php }?> */
        ];

        if(isTrash) {
            menuItems = [
                {
                    text: me.snippets.tree.emptyTrash,
                    iconCls: 'sprite-bin-metal-full',
                    handler: function() {
                        me.fireEvent('emptyTrash', me, view, record, item, index);
                    }
                }
            ];
        }

        var menu = Ext.create('Ext.menu.Menu', {
            items: menuItems
        });

        menu.showAt(event.getPageX(), event.getPageY());
    },

    /**
     * Event listener method which fires when the user performs a right click
     * on the Ext.tree.Panel.
     *
     * Opens a context menu which features functions to create a new album and
     * to reload the album list.
     *
     * Fires the following events on the Ext.tree.Panel:
     * - addAlbum
     * - reload
     *
     * @event containercontextmenu
     * @param [object] view - HTML DOM Object of the Ext.tree.Panel
     * @param [object] event - The fired Ext.EventObject
     * @return void
     */
    onOpenContainerContextMenu: function(view, event) {
        event.preventDefault(true);
        var me = this;

        var menu = Ext.create('Ext.menu.Menu', {
            items: [{
                text: me.snippets.tree.newAlbum,
                iconCls: 'sprite-photo-album--plus',
                handler: function() {
                    me.fireEvent('addAlbum', me, view);
                }
            }, {
                text: me.snippets.tree.refresh,
                iconCls: 'sprite-arrow-circle-315',
                handler: function() {
                    me.fireEvent('reload', me, view);
                }
            }]
        });
        menu.showAt(event.getPageX(), event.getPageY());
    },

    /**
     * Event listener method which fires when the associated
     * Ext.tree.Panel is rendered.
     *
     * Initializes the drop zone for the Ext.tree.Panel to
     * allow moving medias into different albums.
     *
     * @event render
     * @param [object] view - Rendered Ext.tree.Panel
     * @return void
     */
    initializeTreeDropZone: function(view) {
        var treeView = this.view,
            win = view.up('window'),
            mediaView = win.down('mediamanager-media-view');

        view.dropZone = Ext.create('Ext.dd.DropZone', view.getEl(), {
            ddGroup: 'media-tree-dd',
            getTargetFromEvent: function(event) {
                return event.getTarget(treeView.itemSelector);
            },

            onNodeDrop : function(target, dd, e, data) {
                var node = treeView.getRecord(target),
                    models = data.mediaModels,
                    store;

                // The event was fired from the list view
                if(!models) {
                    models = data.records;
                }
                store = models[0].store;

                Ext.each(models, function(model) {
                    model.set('newAlbumID', node.get('id'));
                });

                if (models.length > 1) {
                    view.fireEvent('startBatchMoveMedia', view, models);
                    return;
                }

                mediaView.setLoading(true);

                store.sync({
                    callback: function(){
                        mediaView.setLoading(false);
                        store.load();
                        view.fireEvent('reload');
                    }
                });
            }
        });
    }
});
//
<?php }} ?>