<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:16
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\view\replace\file_select.js" */ ?>
<?php /*%%SmartyHeaderCode:27345bd94fb8611499-66374889%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62a57af7e7d0e7bd308f79e2dc2adc255bde387e' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\replace\\file_select.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27345bd94fb8611499-66374889',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb861af54_74652223',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb861af54_74652223')) {function content_5bd94fb861af54_74652223($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 */

//
//
Ext.define('Shopware.apps.MediaManager.view.replace.FileSelect', {
    extend: 'Ext.form.field.File',

    anchor: '100%',
    margin: '10 0 20 0',
    name: 'images[]',
    labelStyle: 'font-weight: 700',
    labelWidth: 0,
    allowBlank: true,

    buttonConfig: {
        cls: 'secondary small',
        iconCls: 'sprite-inbox-image'
    },

    /**
     * init´s the component
     */
    initComponent: function() {
        var me = this;

        if (me.maxFileUpload > 1) {
            me.on('render', function() {
                me.fileInputEl.set({ multiple: true });
            });
        }

        me.callParent(arguments);
    },

    /**
     * @override
     */
    onFileChange: function(button, e, value) {
        this.duringFileSelect = true;

        var me = this,
            upload = me.fileInputEl.dom,
            files = upload.files,
            names = [];

        if (files) {
            for (var i = 0; i < files.length; i++)
                names.push(files[i].name);
            value = names.join(', ');
        }

        Ext.form.field.File.superclass.setValue.call(this, value);

        delete this.duringFileSelect;
    }
});
//<?php }} ?>