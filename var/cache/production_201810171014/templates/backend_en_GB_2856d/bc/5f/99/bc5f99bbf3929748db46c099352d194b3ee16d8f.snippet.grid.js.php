<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:15
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\view\media\grid.js" */ ?>
<?php /*%%SmartyHeaderCode:320875bd94fb7aec280-05802883%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bc5f99bbf3929748db46c099352d194b3ee16d8f' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\media\\grid.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '320875bd94fb7aec280-05802883',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb7cbdbc2_47724208',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb7cbdbc2_47724208')) {function content_5bd94fb7cbdbc2_47724208($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage View
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Media View
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.MediaManager.view.media.Grid', {
    extend: 'Ext.grid.Panel',
    border: 0,
    bodyBorder: 0,
    alias: 'widget.mediamanager-media-grid',
    /**
     * Selected preview size in px
     * @Number
     * @default 16
     */
    selectedPreviewSize: 16,

    /**
     * Used snippets in this component
     * @object
     */
    snippets: {
        column: {
            'preview': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'preview','default'=>'Preview','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'preview','default'=>'Preview','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Preview<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'preview','default'=>'Preview','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'created': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'created','default'=>'Upload date','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'created','default'=>'Upload date','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Upload date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'created','default'=>'Upload date','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'name': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'name','default'=>'File name','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'name','default'=>'File name','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
File name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'name','default'=>'File name','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'width': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'width','default'=>'Image width','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'width','default'=>'Image width','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Image width<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'width','default'=>'Image width','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'height': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'height','default'=>'Image height','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'height','default'=>'Image height','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Image height<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'height','default'=>'Image height','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'type': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'type','default'=>'File type','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'type','default'=>'File type','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
File type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'type','default'=>'File type','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        types: {
            'video': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'video','default'=>'Video','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'video','default'=>'Video','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Video<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'video','default'=>'Video','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'music': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'music','default'=>'Music','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'music','default'=>'Music','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Music<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'music','default'=>'Music','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'archive': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'archive','default'=>'Archive','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'archive','default'=>'Archive','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Archive<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'archive','default'=>'Archive','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'pdf': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'pdf','default'=>'PDF','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'pdf','default'=>'PDF','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PDF<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'pdf','default'=>'PDF','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'image': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'image','default'=>'Image','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'image','default'=>'Image','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Image<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'image','default'=>'Image','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'vector': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'vector','default'=>'Vector','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'vector','default'=>'Vector','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vector<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'vector','default'=>'Vector','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'unknown': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'types'/'unknown','default'=>'Unknown','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'unknown','default'=>'Unknown','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unknown<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'types'/'unknown','default'=>'Unknown','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and sets the neccessary
     * toolbars and items.
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.addEvents('showDetail');

        me.columns = me.createColumns();
        me.store = me.mediaStore;

        me.viewConfig = {
            plugins: {
                ptype: 'gridviewdragdrop',
                ddGroup: 'media-tree-dd',
                enableDrop: false
            }
        };

        // Set a checkbox model as the selection model for the grid
        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
            listeners: {
                scope: me,
                selectionchange: function(grid, selection) {
                    me.fireEvent('showDetail', grid, selection);
                }
            }
        });

        // Grid plugins
        me.plugins = [
            Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2
            })
        ];

        me.callParent(arguments);
    },

    /**
     * Creates the columns for the list view.
     *
     * @returns { Array }
     */
    createColumns: function() {
        var me = this;

        return [{
            dataIndex: 'thumbnail',
            header: me.snippets.column.preview,
            width: 50,
            align: 'center',
            renderer: me.previewRenderer,
            sortable: false
        }, {
            dataIndex: 'created',
            header: me.snippets.column.created,
            renderer: me.dateRenderer,
            flex: 1
        }, {
            dataIndex: 'name',
            header: me.snippets.column.name,
            flex: 3,
            renderer: me.nameRenderer,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'width',
            header: me.snippets.column.width,
            hidden: true,
            flex: 1,
            renderer: me.pixelRenderer
        }, {
            dataIndex: 'height',
            header:  me.snippets.column.height,
            hidden: true,
            flex: 1,
            renderer: me.pixelRenderer
        }, {
            dataIndex: 'type',
            header: me.snippets.column.type,
            flex: 1,
            renderer: me.typeRenderer
        }]
    },

    /**
     * Renders the preview column. If the entry is an image, the image will be rendered. Otherwise
     * the renderer renders an item (using a `div` box).
     *
     * @param { String } value - The value of the column
     * @param { Object } tdStyle - The style of the `td` element
     * @param { Shopware.apps.MediaManager.model.Media } record - The used record
     * @returns { String } Formatted output
     */
    previewRenderer: function(value, tdStyle, record) {
        var me = this,
            type = record.get('type').toLowerCase(),
            value = value + '?' + new Date().getTime(),
            result;

        switch(type) {
           case 'video':
               result = '<div class="sprite-blue-document-film" style="height:16px; width:16px;display:inline-block"></div>';
               break;
           case 'music':
               result = '<div class="sprite-blue-document-music" style="height:16px; width:16px;display:inline-block"></div>';
               break;
           case 'archive':
               result = '<div class="sprite-blue-document-zipper" style="height:16px; width:16px;display:inline-block"></div>';
               break;
           case 'pdf':
               result = '<div class="sprite-blue-document-pdf-text" style="height:16px; width:16px;display:inline-block"></div>';
               break;
           case 'vector':
               result = '<div class="sprite-blue-document-illustrator" style="height:16px; width:16px;display:inline-block"></div>';
            break;
           case 'image':
               if (Ext.Array.contains(['tif', 'tiff'], record.data.extension)) {
                   result = '<div class="sprite-blue-document-image" style="height:16px; width:16px;display:inline-block"></div>';
               } else {
                   result = Ext.String.format('<div class="small-preview-image"><img src="[0]" style="max-width:[1]px;max-height:[1]px" alt="[2]" /></div>', value, me.selectedPreviewSize, record.get('name'));
               }

               break;
           default:
               result = '<div class="sprite-blue-document-text" style="height:16px; width:16px;display:inline-block"></div>';
               break;
       }

        return result;

    },

    /**
     * Renders the name column of the list view.
     *
     * @param { String } value - The value of the column
     * @param { Object } tdStyle - The style of the `td` element
     * @param { Shopware.apps.MediaManager.model.Media } record - The used record
     * @returns { String } Formatted output
     */
    nameRenderer: function(value, tdStyle, record) {
        value = Ext.String.format('[0].[1]', value, record.get('extension'));
        return value;
    },

    /**
     * Renders the date column of the list view.
     *
     * @param { String } value - The value of the column
     * @returns { String } Formatted output
     */
    dateRenderer: function(value) {
        return Ext.util.Format.date(value);
    },

    /**
     * Renders the width and height of the list view. If the value is empty,
     * an minus ("-") will be returned.
     *
     * @param { String } value - The value of the column
     * @returns { String }
     */
    pixelRenderer: function(value) {
        if(value) {
            return value + 'px';
        }
        return '-';
    },

    /**
     * Renders the type column of the list view. The value will
     * be replaced with a snippet.
     *
     * @param { String } value - The value of the column
     * @returns { String } Formatted output
     */
    typeRenderer: function(value) {
        var result = '',
            type = value.toLowerCase();

        switch(type) {
            case 'pdf': // Explicit fall-throughs
            case 'video':
            case 'music':
            case 'archive':
            case 'image':
            case 'vector':
                result = this.snippets.types[type];
                break;

            default:
                result = this.snippets.types['unknown'];
                break;
        }

        return result;
    }
});
//
<?php }} ?>