<?php /* Smarty version Smarty-3.1.12, created on 2018-10-31 07:46:16
         compiled from "C:\xampp2\htdocs\development\shopware2\themes\Backend\ExtJs\backend\media_manager\view\replace\window.js" */ ?>
<?php /*%%SmartyHeaderCode:108505bd94fb81a3158-43202818%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd550a7751869bfe580321c93b5ade4457f5ab7ef' => 
    array (
      0 => 'C:\\xampp2\\htdocs\\development\\shopware2\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\replace\\window.js',
      1 => 1539751512,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '108505bd94fb81a3158-43202818',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bd94fb8214369_95681469',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bd94fb8214369_95681469')) {function content_5bd94fb8214369_95681469($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 */

//
//
Ext.define('Shopware.apps.MediaManager.view.replace.Window', {
    extend: 'Enlight.app.Window',
    alias: 'widget.mediamanager-replace-window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mediaManager/replaceWindow/window/title','namespace'=>'backend/media_manager/view/replace')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/title','namespace'=>'backend/media_manager/view/replace'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Replace media<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/title','namespace'=>'backend/media_manager/view/replace'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    cancelUrl: '<?php echo '/development/shopware2/backend/MediaManager/cancel';?>',
    replaceUrl: '<?php echo '/development/shopware2/backend/MediaManager/replace';?>',
    updateUrl: '<?php echo '/development/shopware2/backend/MediaManager/updateTemporaryMedia';?>',
    height: 'auto',
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,
    width: 615,
    maxHeight: 500,
    baseHeight: 210,
    rowHeight: 136,

    bodyStyle: {
        background: '#F0F2F4'
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.items = me.createItems();
        me.height = me.getHeight();
        me.dockedItems = me.createDockedItems();
        me.registerEvents();

        me.callParent(arguments);
    },

    /**
     * Creates all items
     *
     * @return { Array }
     */
    createItems: function() {
        var me = this;

        return [
            me.createInfoPanel(),
            me.createReplaceGrid()
        ]
    },

    /**
     * Registers the required events
     */
    registerEvents: function() {
        var me = this;

        me.replaceGrid.on('uploadReady', me.startUpload, me);
        me.replaceGrid.on('upload-error', me.onError, me);
    },

    /**
     * Calculates the height of the window
     *
     * @return { number }
     */
    getHeight: function() {
        var me = this;

        return me.baseHeight + (me.rowHeight * me.selectedMedias.length);
    },

    /**
     * Creates the replace grid with all selected medias
     *
     * @return { Shopware.apps.MediaManager.view.replace.Grid }
     */
    createReplaceGrid: function() {
        var me = this;

        me.replaceGrid = Ext.create('Shopware.apps.MediaManager.view.replace.Grid', {
            selectedMedias: me.selectedMedias
        });

        return me.replaceGrid;
    },

    /**
     * Creates the docked items
     *
     * @return { Array }
     */
    createDockedItems: function() {
        var me = this;

        return [
            me.createBottomToolbar()
        ];
    },

    /**
     * Creates the info field set
     *
     * @return { Ext.form.FieldSet }
     */
    createInfoPanel: function() {
        var info = Ext.create('Ext.container.Container', {
            html: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mediaManager/replaceWindow/window/infoText','namespace'=>'backend/media_manager/view/replace')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/infoText','namespace'=>'backend/media_manager/view/replace'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
The selected media file will be replaced. All links will remain. Possible links are item images, blog posts or shopping worlds. The thumbnails will be regenerated automatically after replacing. <strong>Note:</strong> The file name and the destination will not be changed by replacing.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/infoText','namespace'=>'backend/media_manager/view/replace'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        return Ext.create('Ext.form.FieldSet', {
            margin: '10 10 20 10',
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mediaManager/replaceWindow/window/infoHeader','namespace'=>'backend/media_manager/view/replace')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/infoHeader','namespace'=>'backend/media_manager/view/replace'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<b>Important information</b><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/infoHeader','namespace'=>'backend/media_manager/view/replace'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [
                info
            ]
        })
    },

    /**
     * Creates the toolbar with cancel and save button
     *
     * @return { Ext.toolbar.Toolbar }
     */
    createBottomToolbar: function() {
        var me = this;

        return Ext.create('Ext.toolbar.Toolbar', {
            dock: 'bottom',
            ui: 'shopware-ui',
            cls: 'shopware-toolbar',
            items: [
                '->',
                me.createCancelButton(),
                me.createSaveButton()
            ]
        });
    },

    /**
     * Creates the save button
     *
     * @return { Ext.button.Button }
     */
    createSaveButton: function() {
        var me = this;

        return Ext.create('Ext.button.Button', {
            cls: 'primary',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mediaManager/replaceWindow/window/save','namespace'=>'backend/media_manager/view/replace')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/save','namespace'=>'backend/media_manager/view/replace'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/save','namespace'=>'backend/media_manager/view/replace'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler: Ext.bind(me.onClickSaveButton, me)
        });
    },

    /**
     * creates the cancel button
     *
     * @return { Ext.button.Button }
     */
    createCancelButton: function() {
        var me = this;

        return Ext.create('Ext.button.Button', {
            cls: 'secondary',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mediaManager/replaceWindow/window/cancel','namespace'=>'backend/media_manager/view/replace')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/cancel','namespace'=>'backend/media_manager/view/replace'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/cancel','namespace'=>'backend/media_manager/view/replace'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler: Ext.bind(me.close, me)
        });
    },

    /**
     * the save button handler
     *
     * @param { Ext.button.Button } button
     */
    onClickSaveButton: function(button) {
        var me = this;

        me.rowIndex = 0;

        me.startUpload();
    },

    /**
     * starts the upload of the selected files and shows a growlMessage if the upload is ready
     */
    startUpload: function() {
        var me = this,
            mediaManager = me.mediaManager,
            selectedRecord = mediaManager.dataView.getSelectionModel().getSelection()[0],
            length = me.replaceGrid.rows.length,
            rows = me.replaceGrid.rows,
            row;

        if (mediaManager.selectedLayout === 'table') {
            selectedRecord = mediaManager.down('mediamanager-media-grid').getSelectionModel().getSelection()[0];
        }

        me.setLoading(true);

        if (me.rowIndex >= length) {
            Shopware.Notification.createGrowlMessage(
                '',
                '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mediaManager/replaceWindow/window/saved','namespace'=>'backend/media_manager/view/replace')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/saved','namespace'=>'backend/media_manager/view/replace'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Your changes have been saved.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mediaManager/replaceWindow/window/saved','namespace'=>'backend/media_manager/view/replace'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            );
            mediaManager.mediaStore.load({
                callback: function() {
                    if (!selectedRecord) {
                        return;
                    }

                    var record = mediaManager.mediaStore.getById(selectedRecord.get('id'));
                    if (record) {
                        mediaManager.infoView.update(record.getData());
                    }
                }
            });


            me.close();
            return;
        }

        row = rows[me.rowIndex];
        me.rowIndex++;
        row.startUpload();
    },

    /**
     * Event handler was called if a error occurred
     */
    onError: function() {
        var me = this;

        me.setLoading(false);
    }
});
//
<?php }} ?>