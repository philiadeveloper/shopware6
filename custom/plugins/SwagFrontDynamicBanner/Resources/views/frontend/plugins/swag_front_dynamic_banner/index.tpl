{extends file="frontend/index/index.tpl"}
{block name="frontend_home_index_banner"}
{$smarty.block.parent}
<div id="frontBannerParent">
	<img src="{$mainfrontbanner1}" id="frontBannerImage">
	<div id="bannerDiv">
	    <div class="outer">
		    <div class="inner rotate">
				<div id="frontBannerText1">{$mainfrontbannertext1}</div>
				<div id="frontBannerText2">{$mainfrontbannertext1_2}</div>
				<div id="frontBannerText3">{$mainfrontbannerdescription1}</div>
				<a class="philia_front_banner_text_link" href="{$mainfrontbannerlink1}">{$mainfrontbannerlinktext1}</a>
		    </div>
		</div>
	</div>
</div>

{/block}