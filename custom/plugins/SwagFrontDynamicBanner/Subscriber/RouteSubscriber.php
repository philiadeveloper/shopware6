<?php
namespace SwagFrontDynamicBanner\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\ConfigReader;
use SwagFrontDynamicBanner\Components\SloganPrinter;

class RouteSubscriber implements SubscriberInterface
{
    private $pluginDirectory;
    private $sloganPrinter;
    private $config;

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch'
        ];
    }

    public function __construct($pluginName, $pluginDirectory, SloganPrinter $sloganPrinter, ConfigReader $configReader)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->sloganPrinter = $sloganPrinter;

        $this->config = $configReader->getByPluginName($pluginName);
    }

    public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        $view->extendsTemplate('frontend/plugins/swag_front_dynamic_banner/index.tpl');


        $view->assign('mainfrontbanner1', $this->config['mainfrontbanner1']);
        $view->assign('mainfrontbannertext1', $this->config['mainfrontbannertext1']);
        $view->assign('mainfrontbannertext1_2', $this->config['mainfrontbannertext1_2']);
        $view->assign('mainfrontbannerdescription1', $this->config['mainfrontbannerdescription1']);
        $view->assign('mainfrontbannerlink1', $this->config['mainfrontbannerlink1']);
        $view->assign('mainfrontbannerlinktext1', $this->config['mainfrontbannerlinktext1']);


        /*$view->assign('mainfrontbanner2', $this->config['mainfrontbanner2']);
        $view->assign('mainfrontbannertext2', $this->config['mainfrontbannertext2']);
        $view->assign('mainfrontbannertext2_2', $this->config['mainfrontbannertext2_2']);
        $view->assign('mainfrontbannerdescription2', $this->config['mainfrontbannerdescription2']);
        $view->assign('mainfrontbannerlink2', $this->config['mainfrontbannerlink2']);
        $view->assign('mainfrontbannerlinktext2', $this->config['mainfrontbannerlinktext2']);

        $view->assign('mainfrontbanner3', $this->config['mainfrontbanner3']);
        $view->assign('mainfrontbannertext3', $this->config['mainfrontbannertext3']);
        $view->assign('mainfrontbannertext3_2', $this->config['mainfrontbannertext3_2']);
        $view->assign('mainfrontbannerdescription3', $this->config['mainfrontbannerdescription3']);
        $view->assign('mainfrontbannerlink3', $this->config['mainfrontbannerlink3']);
        $view->assign('mainfrontbannerlinktext3', $this->config['mainfrontbannerlinktext3']);

        $view->assign('mainfrontbanner4', $this->config['mainfrontbanner4']);
        $view->assign('mainfrontbannertext4', $this->config['mainfrontbannertext4']);
        $view->assign('mainfrontbannertext4_2', $this->config['mainfrontbannertext4_2']);
        $view->assign('mainfrontbannerdescription4', $this->config['mainfrontbannerdescription4']);
        $view->assign('mainfrontbannerlink4', $this->config['mainfrontbannerlink4']);
        $view->assign('mainfrontbannerlinktext4', $this->config['mainfrontbannerlinktext4']);

        $view->assign('mainfrontbanner5', $this->config['mainfrontbanner5']);
        $view->assign('mainfrontbannertext5', $this->config['mainfrontbannertext5']);
        $view->assign('mainfrontbannertext5_2', $this->config['mainfrontbannertext5_2']);
        $view->assign('mainfrontbannerdescription5', $this->config['mainfrontbannerdescription5']);
        $view->assign('mainfrontbannerlink5', $this->config['mainfrontbannerlink5']);
        $view->assign('mainfrontbannerlinktext5', $this->config['mainfrontbannerlinktext5']);



*/



        /*if (!$this->config['swagSloganContent']) {
            $view->assign('swagSloganContent', $this->sloganPrinter->getSlogan());
        }*/
    }
}
