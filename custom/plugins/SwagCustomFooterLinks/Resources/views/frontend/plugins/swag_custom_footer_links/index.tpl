{extends file="frontend/index/index.tpl"}
{block name="frontend_index_footer_custom_philia_links"}
{$smarty.block.parent}
{namespace name="frontend/index/menu_footer"}
<div class="philia_custom_footer_container_main">
	<div class="philia_custom_footer_container_main_first">
		<div class="philia_custom_footer_container_main_first_wrapper">
			<div>
				{block name="frontend_index_footer_column_service_hotline"}
				
				<!-- {block name="frontend_index_footer_column_service_hotline_headline"}
				<div class="column--headline">{s name="sFooterServiceHotlineHead"}{/s}</div>
				{/block} -->
				{block name="frontend_index_footer_column_service_hotline_content"}
				<div class="column--content">
					<p class="column--desc">{$hotline}</p>
				</div>
				{/block}
				{/block}
			</div>
		</div>
	</div>
	<div class="philia_custom_footer_container_main_second">
		
		<!-- {block name="frontend_index_footer_column_service_menu_headline"}
		<div class="column--headline">{s name="sFooterShopNavi1"}{/s}</div>
		{/block} -->
		{block name="frontend_index_footer_column_service_menu_content"}
		<nav class="column--navigation column--content">
			<ul class="navigation--list" role="menu">
				{block name="frontend_index_footer_column_service_menu_before"}{/block}
				
				{block name="frontend_index_footer_column_service_menu_entry"}
				{if $navigation1 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link1}">{$navigation1}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation2 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link2}">{$navigation2}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation3 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link3}">{$navigation3}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation4 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link4}">{$navigation4}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation5 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link5}">{$navigation5}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation6 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link6}">{$navigation6}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation7 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link7}">{$navigation7}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{/block}
				
				{block name="frontend_index_footer_column_service_menu_after"}{/block}
			</ul>
		</nav>
		{/block}
		
	</div>
	<div class="philia_custom_footer_container_main_third">
		<nav class="column--navigation column--content">
			<ul class="navigation--list" role="menu">
				{block name="frontend_index_footer_column_service_menu_before"}{/block}
				
				{block name="frontend_index_footer_column_service_menu_entry"}
				{if $navigation8 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link8}">{$navigation8}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation9 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link9}">{$navigation9}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation10 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link10}">{$navigation10}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation11 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link11}">{$navigation11}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation12 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link12}">{$navigation12}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation13 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link13}">{$navigation13}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{if $navigation14 != ''}
				<li class="navigation--entry" role="menuitem" style="position: relative;"><a class="navigation--link" href="{$link14}">{$navigation14}</a><i style="position: absolute;right:0;margin-top: -2px;font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></li>
				{/if}
				{/block}
				
				{block name="frontend_index_footer_column_service_menu_after"}{/block}
			</ul>
		</nav>
	</div>
</div>
{/block}