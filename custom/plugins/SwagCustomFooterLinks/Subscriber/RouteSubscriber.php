<?php
namespace SwagCustomFooterLinks\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\ConfigReader;
use SwagCustomFooterLinks\Components\SloganPrinter;

class RouteSubscriber implements SubscriberInterface
{
    private $pluginDirectory;
    private $sloganPrinter;
    private $config;

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch'
        ];
    }

    public function __construct($pluginName, $pluginDirectory, SloganPrinter $sloganPrinter, ConfigReader $configReader)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->sloganPrinter = $sloganPrinter;

        $this->config = $configReader->getByPluginName($pluginName);
    }

    public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        $view->extendsTemplate('frontend/plugins/swag_custom_footer_links/index.tpl');
        $view->assign('hotline', $this->config['hotline']);
        $view->assign('navigation1', $this->config['navigation1']);
        $view->assign('navigation2', $this->config['navigation2']);
        $view->assign('navigation3', $this->config['navigation3']);
        $view->assign('navigation4', $this->config['navigation4']);
        $view->assign('navigation5', $this->config['navigation5']);
        $view->assign('navigation6', $this->config['navigation6']);
        $view->assign('navigation7', $this->config['navigation7']);
        $view->assign('navigation8', $this->config['navigation8']);
        $view->assign('navigation9', $this->config['navigation9']);
        $view->assign('navigation10', $this->config['navigation10']);
        $view->assign('navigation11', $this->config['navigation11']);
        $view->assign('navigation12', $this->config['navigation12']);
        $view->assign('navigation13', $this->config['navigation13']);
        $view->assign('navigation14', $this->config['navigation14']);


        $view->assign('link1', $this->config['link1']);
        $view->assign('link2', $this->config['link2']);
        $view->assign('link3', $this->config['link3']);
        $view->assign('link4', $this->config['link4']);
        $view->assign('link5', $this->config['link5']);
        $view->assign('link6', $this->config['link6']);
        $view->assign('link7', $this->config['link7']);
        $view->assign('link8', $this->config['link8']);
        $view->assign('link9', $this->config['link9']);
        $view->assign('link10', $this->config['link10']);
        $view->assign('link11', $this->config['link11']);
        $view->assign('link12', $this->config['link12']);
        $view->assign('link13', $this->config['link13']);
        $view->assign('link14', $this->config['link14']);

        /*if (!$this->config['swagSloganContent']) {
            $view->assign('swagSloganContent', $this->sloganPrinter->getSlogan());
        }*/
    }
}
