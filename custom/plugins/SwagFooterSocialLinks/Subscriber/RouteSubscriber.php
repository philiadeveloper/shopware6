<?php
namespace SwagFooterSocialLinks\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\ConfigReader;
use SwagFooterSocialLinks\Components\SloganPrinter;

class RouteSubscriber implements SubscriberInterface
{
    private $pluginDirectory;
    private $sloganPrinter;
    private $config;

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch'
        ];
    }

    public function __construct($pluginName, $pluginDirectory, SloganPrinter $sloganPrinter, ConfigReader $configReader)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->sloganPrinter = $sloganPrinter;

        $this->config = $configReader->getByPluginName($pluginName);
    }

    public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        $view->extendsTemplate('frontend/plugins/swag_footer_social_links/index.tpl');
        $view->assign('socialimage1', $this->config['image1']);
        $view->assign('socialimage2', $this->config['image2']);
        $view->assign('socialimage3', $this->config['image3']);
        $view->assign('socialimage4', $this->config['image4']);
        $view->assign('socialimage5', $this->config['image5']);
        $view->assign('socialimage6', $this->config['image6']);
        $view->assign('socialimage7', $this->config['image7']);
        $view->assign('socialimage8', $this->config['image8']);
        $view->assign('socialimage9', $this->config['image9']);
        $view->assign('socialimage10', $this->config['image10']);
        $view->assign('socialimagelink1', $this->config['imagelink1']);
        $view->assign('socialimagelink2', $this->config['imagelink2']);
        $view->assign('socialimagelink3', $this->config['imagelink3']);
        $view->assign('socialimagelink4', $this->config['imagelink4']);
        $view->assign('socialimagelink5', $this->config['imagelink5']);
        $view->assign('socialimagelink6', $this->config['imagelink6']);
        $view->assign('socialimagelink7', $this->config['imagelink7']);
        $view->assign('socialimagelink8', $this->config['imagelink8']);
        $view->assign('socialimagelink9', $this->config['imagelink9']);
        $view->assign('socialimagelink10', $this->config['imagelink10']);
        $view->assign('socialiconsize', $this->config['socialiconsize']);

        /*if (!$this->config['swagSloganContent']) {
            $view->assign('swagSloganContent', $this->sloganPrinter->getSlogan());
        }*/
    }
}
