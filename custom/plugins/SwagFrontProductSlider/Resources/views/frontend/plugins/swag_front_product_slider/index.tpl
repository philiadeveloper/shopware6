{extends file="frontend/index/index.tpl"}
{block name="frontend_index_content_philia_slider"}
{$smarty.block.parent}
<center>
<div style="margin: auto;width: 85%;">
<div class="philia_first_text_heading1">{$heading_philia_slider1}</div>
<div class="philia_first_text_heading2">{$heading_philia_slider2}</div>
<br>
<br>
<div id="owl-demo" class="owl-carousel owl-theme">
{foreach from=$products key=k item=v}        
<div class="item">
{if $v['product_percent']!=0}	
<div style="transform: rotate(-20deg);z-index:999;padding-right:10px;padding-left:10px;padding:5px;background: #c31d17;position: absolute;left: 30px;color: white;">{$v['product_percent']}%</div>	
{/if}
<a href="{$v['product_url']}">
<div class="philia_product_slider">
<img src="{$v['product_image']}" class="philia_product_slider_image">
</div>
<br>
<div class="philia_product_slider_text">{$v['product_name']}</div><br>
<div class="philia_product_slider_text"><i class="fa fa-usd" aria-hidden="true"></i>{$v['product_price']}</div>
</a>
</div>
 {/foreach}
</div>
	
</div>
</center>

{/block}