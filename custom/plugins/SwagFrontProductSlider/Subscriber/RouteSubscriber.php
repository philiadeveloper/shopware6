<?php
namespace SwagFrontProductSlider\Subscriber;


use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\ConfigReader;
use Shopware\Models\Article\Article;
use SwagFrontProductSlider\Components\SloganPrinter;

class RouteSubscriber implements SubscriberInterface
{
    private $pluginDirectory;
    private $sloganPrinter;
    private $config;

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch'
        ];
    }

    public function __construct($pluginName, $pluginDirectory, SloganPrinter $sloganPrinter, ConfigReader $configReader)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->sloganPrinter = $sloganPrinter;

        $this->config = $configReader->getByPluginName($pluginName);
    }

    public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        $view->extendsTemplate('frontend/plugins/swag_front_product_slider/index.tpl');
        $view->assign('heading_philia_slider1', $this->config['heading_philia_slider1']);
        $view->assign('heading_philia_slider2', $this->config['heading_philia_slider2']);
        $view->assign('product_slider_type', $this->config['product_slider_type']);
        $view->assign('product_id', $this->config['product_id']);
        $view->assign('product_limit', $this->config['product_limit']);
        $product_id = $this->config['product_id'];
        $limit = $this->config['product_limit'];
        $type = $this->config['product_slider_type'];
        if($type=='New Products'){
        	$this->products = Shopware()->Db()->fetchAll("SELECT id,name FROM s_articles WHERE active=1 LIMIT $limit");
        }else{
        	$this->products = Shopware()->Db()->fetchAll("SELECT id,name FROM s_articles WHERE active=1 ORDER BY id DESC LIMIT $limit");
        }
        if($product_id!=''){
        	$this->products = Shopware()->Db()->fetchAll("SELECT id,name FROM s_articles WHERE id IN ($product_id) AND active=1 LIMIT $limit");
        }
        foreach($this->products as $product){
            $this->product_price = Shopware()->Db()->fetchAll("SELECT price,percent FROM s_articles_prices WHERE articleID='".$product['id']."'");
            $imgs = Shopware()->Modules()->Articles()->sGetArticleById($product['id']);
            if(!empty($imgs['image']['source'])){
                $img = $imgs['image']['source'];
            }else{
                $img = '';
            }
            if(round($this->product_price[0]['percent']) > 0){
                $percent = round($this->product_price[0]['percent']);
            }else{
                $percent = 0;
            }
            //print_r($imgs['pseudopricePercent']);
            $pro[] = array(
                'product_id' => $product['id'],
                'product_name' => $product['name'],
                'product_image' => $img,
                'product_url' => $imgs['linkDetailsRewrited'],
                'product_percent' => $percent,
                'product_price' => number_format($imgs['price_numeric'],2),
            );
        }



       $view->assign('products', $pro);
       //echo "<pre>";print_r(Shopware()->Modules()->Articles()->sGetArticleById(1));

        
    }

}
