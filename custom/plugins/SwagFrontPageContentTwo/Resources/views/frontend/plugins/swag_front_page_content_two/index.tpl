{extends file="frontend/index/index.tpl"}
{block name="frontend_index_content_philia2"}
{$smarty.block.parent}
<div class="philia_second_main_container" style="background: {$backgroundColorTwo}">
    <div class="philia_second_main_text_container">
        <div class="philia_second_main_text_holder">
            <div class="philia_second_heading_one">{$heading1}</div>
            <div class="philia_second_heading_two">{$heading2}</div>
            <div class="philia_second_description_one">{$description1}</div>
            <div class="philia_second_description_two">{$description2}</div>
            <a href="{$link}" class="philia_second_link">{$linktext}</a>
        </div>
    </div>
</div>
{/block}