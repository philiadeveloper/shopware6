<?php
namespace SwagFrontPageContentTwo\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\ConfigReader;
use SwagFrontPageContentTwo\Components\SloganPrinter;

class RouteSubscriber implements SubscriberInterface
{
    private $pluginDirectory;
    private $sloganPrinter;
    private $config;

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch'
        ];
    }

    public function __construct($pluginName, $pluginDirectory, SloganPrinter $sloganPrinter, ConfigReader $configReader)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->sloganPrinter = $sloganPrinter;

        $this->config = $configReader->getByPluginName($pluginName);
    }

    public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        $view->extendsTemplate('frontend/plugins/swag_front_page_content_two/index.tpl');
        $view->assign('heading1', $this->config['philia_heading1']);
        $view->assign('heading2', $this->config['philia_heading2']);
        $view->assign('description1', $this->config['philia_description1']);
        $view->assign('description2', $this->config['philia_description2']);
        $view->assign('linktext', $this->config['philia_linktext']);
        $view->assign('link', $this->config['philia_link']);
        $view->assign('backgroundColorTwo', $this->config['philia_backgroundColor']);
        /*if (!$this->config['swagSloganContent']) {
            $view->assign('swagSloganContent', $this->sloganPrinter->getSlogan());
        }*/
    }
}
