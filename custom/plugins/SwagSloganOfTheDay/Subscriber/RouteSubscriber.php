<?php
namespace SwagSloganOfTheDay\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\ConfigReader;
use SwagSloganOfTheDay\Components\SloganPrinter;

class RouteSubscriber implements SubscriberInterface
{
    private $pluginDirectory;
    private $sloganPrinter;
    private $config;

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch'
        ];
    }

    public function __construct($pluginName, $pluginDirectory, SloganPrinter $sloganPrinter, ConfigReader $configReader)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->sloganPrinter = $sloganPrinter;

        $this->config = $configReader->getByPluginName($pluginName);
    }

    public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_Controller_Action $controller */
        $controller = $args->get('subject');
        $view = $controller->View();
        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        $view->extendsTemplate('frontend/plugins/swag_slogan_of_the_day/index.tpl');
        $view->assign('heading1', $this->config['heading1']);
        $view->assign('heading2', $this->config['heading2']);
        $view->assign('banner1text', $this->config['banner1text']);
        $view->assign('image1', $this->config['image1']);
        $view->assign('banner2text', $this->config['banner2text']);
        $view->assign('image2', $this->config['image2']);
        $view->assign('banner3text', $this->config['banner3text']);
        $view->assign('image3', $this->config['image3']);
        /*if (!$this->config['swagSloganContent']) {
            $view->assign('swagSloganContent', $this->sloganPrinter->getSlogan());
        }*/
    }
}
