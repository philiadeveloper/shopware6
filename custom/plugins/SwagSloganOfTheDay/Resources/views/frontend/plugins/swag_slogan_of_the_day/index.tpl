{extends file="frontend/index/index.tpl"}
{block name="frontend_index_content_philia1"}
{$smarty.block.parent}
<div class="slogan-box" style="width:100%;">
    <center><p class="heading1">{$heading1}</p>
    <p class="heading2">{$heading2}</p></center>
    <div class="philia_b1_container">
        <div class="philia_b1_container_image_container">
            <img class="philia_b1_container_image" src="{$image1}">
            <div class="philia_b1_container_text">{$banner1text}</div>
        </div>
        <div class="philia_b1_container_image_container">
            <img class="philia_b1_container_image" src="{$image2}">
            <div class="philia_b1_container_text">{$banner2text}</div>
        </div>
        <div class="philia_b1_container_image_container">
            <img class="philia_b1_container_image" src="{$image3}">
            <div class="philia_b1_container_text">{$banner3text}</div>
        </div>
    </div>
</div>
{/block}