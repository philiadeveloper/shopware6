{extends file="frontend/index/index.tpl"}
{block name="frontend_index_content_philia2"}
{$smarty.block.parent}
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<div style="background: {$backgroundColor};" class="philia_first_main_container">

<div class="philia_first_main_container_first">
	
	<div class="philia_first_image_container">
		<img src="{$image}" class="philia_first_image">
	</div>
	<div style="background: {$backgroundColor};" class="philia_first_text_container">
		<div class="philia_first_text_heading1">{$heading1}</div>
		<div class="philia_first_text_heading2">{$heading2}</div>
		<div class="philia_first_text_description">{$description}</div>
		<a class="philia_first_text_link" href="{$link}">{$linktext}</a>
	</div>
</div>

</div>

{/block}